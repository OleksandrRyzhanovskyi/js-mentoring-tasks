import { Bookshelf } from './src/Bookshelf/Bookshelf';
import { Character, Person } from './src/entity';
import { Book } from './src/Book/Book';
import { http } from './src/HttpClient/HttpClient';

const newBook1 = new Book({ name: 'Oleksandr', age: 21 } as Character);
const newBook2 = new Book({ name: 'Victor', age: 35 } as Character);
const newBook3 = new Book({ name: 'Andrii', age: 43 } as Character);
const bookShelfInstance = new Bookshelf([newBook1, newBook2, newBook3]);

console.log(bookShelfInstance.readStory());
console.log(bookShelfInstance.readStory());
console.log(bookShelfInstance.readStory());

const url = 'https://swapi.co/api/people/';

http.get<Person>(`${url}1`).then(res => console.log(res.eye_color, res.hair_color));

const promises: Promise<Person>[] = [];
const urls: string[] = [];

for (let i = 1; i < 10; i++) {
    promises.push(http.get<Person>(url + i));
    urls.push(url + 1);
}

Promise.all(promises).then(resArr => resArr.forEach((hero: Person) => console.log(hero.name)));
// @ts-ignore
Promise.all(urls.map(http.get)).then(items => console.log(items));
