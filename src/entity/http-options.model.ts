export interface HttpOptions {
    [key: string]: string;
}
