import { HttpOptions } from './http-options.model';

export abstract class BaseHttp {

    abstract get: <T>(url: string, headers?: HttpOptions) => Promise<T>;
    abstract remove: <T>(url: string, headers: HttpOptions) => Promise<T>;
    abstract post: <T>(url: string, headers: HttpOptions, body: T) => Promise<T>;
    abstract put: <T>(url: string, headers: HttpOptions, body: T) => Promise<T>;

    makeRequest(method: string, url: string, options?: HttpOptions): XMLHttpRequest {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        return this.createHeaders(options, xhr);
    }

    createHeaders(options: HttpOptions, xmlHttpRequest: XMLHttpRequest): XMLHttpRequest {
        for (const option in options) {
            xmlHttpRequest.setRequestHeader(option, options[option]);
        }
        return xmlHttpRequest;
    }

}
