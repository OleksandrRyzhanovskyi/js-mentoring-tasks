import { HttpOptions, BaseHttp } from '../entity';

class HttpClient extends BaseHttp {
    public get = <T>(url: string, headers?: HttpOptions) => {
        const req: Promise<T> = new Promise((resolve, reject) => {
            const xhr = this.makeRequest('GET', url, headers);
            xhr.onreadystatechange = () => {
                if (xhr.readyState !== 4) return;

                if (xhr.status >= 200 && xhr.status < 400) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(JSON.parse(xhr.response));
                }
            };
            xhr.send();
        });
        return req;
    }

    public post = <T>(url: string, headers?: HttpOptions, body?: T) => {
        const req: Promise<T> = new Promise((resolve, reject) => {
            const xhr = this.makeRequest('POST', url, headers);
            xhr.onreadystatechange = () => {
                if (xhr.readyState !== 4) return;

                if (xhr.status >= 200 && xhr.status < 400) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(JSON.parse(xhr.response));
                }
            };
            xhr.send(JSON.stringify(body));
        });
        return req;
    }

    public put = <T>(url: string, headers?: HttpOptions, body?: T) => {
        const req: Promise<T> = new Promise((resolve, reject) => {
            const xhr = this.makeRequest('PUT', url, headers);
            xhr.onreadystatechange = () => {
                if (xhr.readyState !== 4) return;

                if (xhr.status >= 200 && xhr.status < 400) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(JSON.parse(xhr.response));
                }
            };
            xhr.send(JSON.stringify(body));
        });
        return req;
    }

    public remove = <T>(url: string, headers?: HttpOptions) => {
        const req: Promise<T> = new Promise((resolve, reject) => {
            const xhr = this.makeRequest('DELETE', url, headers);
            xhr.onreadystatechange = () => {
                if (xhr.readyState !== 4) return;

                if (xhr.status >= 200 && xhr.status < 400) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(JSON.parse(xhr.response));
                }
            };
            xhr.send();
        });
        return req;
    }
}

export const http = new HttpClient();
