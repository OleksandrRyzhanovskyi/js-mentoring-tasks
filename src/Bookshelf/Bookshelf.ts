import { Book } from '../Book/Book';

export class Bookshelf {
    private currentBookIndex: number = 0;
    private storyTellerGenerator: IterableIterator<string>;

    constructor(
        private books: Book[] = [],
    ) {}

    addBook(newBook: Book): void {
        this.books.push(newBook);
    }

    private *storyReader(): IterableIterator<string> {
        if (this.currentBookIndex < this.books.length - 1) {
            for (let i = 0; i < this.books.length; i++) {
                yield [...this.books[this.currentBookIndex++]].join('');
            }
        } else {
            this.currentBookIndex = prompt('there are no new books for you, read again ?') ?
                0 : this.currentBookIndex;
        }
    }

    readStory() {
        if (this.storyTellerGenerator) {
            return this.storyTellerGenerator.next();
        }
        this.storyTellerGenerator = this.storyReader();

        return this.storyTellerGenerator.next();
    }
}
