import { Character } from '../entity';

export class Book {
    constructor(
        private character: Character,
    ) {}

    [Symbol.iterator]() {
        const story: string[] = this.storyText.split('\n');
        let currentIndex = 0;
        return {
            next() {
                if (currentIndex < story.length - 1) {
                    const res = { value: story[currentIndex], done: false };
                    currentIndex++;

                    return res;
                }

                return { value: story[currentIndex], done: true };
            },
        };
    }

    private storyText: string = `This is a story about ${this.character.name} he was ${this.character.age} years old,
       and was working as a frontend developer in a big company,
       he has been working with several frameworks since he was hired,
       had some experience with React and Angular 2+,
       however currently he has to develop on Dojo 1.9 and hopes to leave the project
       to switch technologies as soon as it is possible`;
}
