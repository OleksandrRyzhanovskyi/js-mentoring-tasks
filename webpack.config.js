const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: path.join(__dirname, './index.ts'),
    output: {
        filename: './dist/index.js',
        path: __dirname
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: './dist/index.html'
        })
    ],
    module: {
        rules: [
            {
                test: /\.(ts)$/,
                enforce: 'pre',
                loader: 'tslint-loader'
            },
            {
                test: /\.(ts)$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
        ]
    },
    resolve: {
        extensions: ['.ts', '.js']
    }
};
